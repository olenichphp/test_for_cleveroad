<?php

class WebUser extends CWebUser {

    private $_model = null;
 
    function getRole() {
        if ($user = $this->getModel()) {
            $roleForRead = array(1 => 'user', 2 => 'manager');
            return $roleForRead[$user->role];
        }
    }

    private function getModel() {
        if (!$this->isGuest && $this->_model === null) {
            $this->_model = TblUser::model()->findByPk($this->id, array('select' => 'role'));
        }
        return $this->_model;
    }

}
