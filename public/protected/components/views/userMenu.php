<nav class="navbar navbar-default" role="navigation">
    <div class="container-fluid">
            <?php
            $this->widget('zii.widgets.CMenu', array(
               'items'=>array(
                        array('label'=>'Главная', 'url'=>array('/post/view')),
                        array('label'=>'Регистрация', 'url'=>array('/user/registration'), 'visible'=>Yii::app()->user->isGuest),
                        array('label'=>'Вход', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),
                        array('label'=>'Мои товары', 'url'=>array('/post/mygoods'), 'visible'=>!Yii::app()->user->isGuest),
                        array('label'=>'Редактировать профиль', 'url'=>array('/user/settings'), 'visible'=>!Yii::app()->user->isGuest),
                        array('label'=>'Выход ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest)
                ),
                'itemTemplate' => '{menu}',
                'htmlOptions' => array('class' => 'nav navbar-nav'),
                'submenuHtmlOptions' => array('class' => 'dropdown-menu'),
                'encodeLabel'=>false
            ));
            ?>
    </div>
</nav>