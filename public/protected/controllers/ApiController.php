<?php

class ApiController extends Controller {


    /**
     * Key which has to be in HTTP USERNAME and PASSWORD headers 
     */
    Const APPLICATION_ID = 'ASCCPE';

    /**
     * Default response format
     * either 'json' or 'xml'
     */
    private $format = 'json';
    
    /*
     * id пользователя
     */
    private $userId;

    /**
     * @return array action filters
     */
    public function filters() {
        return array();
    }

    /*
     * получить список всех товаров
     */

    public function actionList() {
        switch ($_GET['model']) {
            case 'posts':
                $models = TblPost::model()->findAll();
                break;
            default:
                // Model not implemented error
                $this->_sendResponse(501, sprintf(
                                'Error: Mode <b>list</b> is not implemented for model <b>%s</b>', $_GET['model']));
                Yii::app()->end();
        }

        // Did we get some results?
        if (empty($models)) {
            // No
            $this->_sendResponse(200, sprintf('No items where found for model <b>%s</b>', $_GET['model']));
        } else {
            // Prepare response
            $rows = array();
            foreach ($models as $model)
                $rows[] = $model->attributes;
            // Send the response
            $this->_sendResponse(200, CJSON::encode($rows));
        }
    }

    public function actionView() {
        // Check if id was submitted via GET
        if (!isset($_GET['id']))
            $this->_sendResponse(500, 'Error: Parameter <b>id</b> is missing');

        switch ($_GET['model']) {
            // Find respective model    
            case 'posts':
                $model = TblPost::model()->findByPk($_GET['id']);
                break;
            case 'users':
                $model = TblUser::model()->findByPk($_GET['id'], array('select' => 'id, username, email'));
                break;
            default:
                $this->_sendResponse(501, sprintf(
                                'Mode <b>view</b> is not implemented for model <b>%s</b>', $_GET['model']));
                Yii::app()->end();
        }

        if (is_null($model))
            $this->_sendResponse(404, 'No Item found with id ' . $_GET['id']);
        else {
            $res = $model->attributes;
            $this->_sendResponse(200, CJSON::encode(array_diff($res, array(''))));
        }
    }

    public function actionCreate() {

        $this->_checkAuth();
        switch ($_GET['model']) {
            // Get an instance of the respective model
            case 'posts':
                $model = new TblPost;
                break;
            default:
                $this->_sendResponse(501, sprintf('Mode <b>create</b> is not implemented for model <b>%s</b>', $_GET['model']));
                Yii::app()->end();
        }

        if ($_POST['title']) {
            $model->title = $_POST['title'];
        } else {
            $this->_sendResponse(500, sprintf('Parameter <b>title</b> is not > is not not available'));
        }
        if ($_POST['price']) {
            $model->price = $_POST['price'];
        } else {
            $this->_sendResponse(500, sprintf('Parameter <b>price</b> is not > is not not available'));
        }
        if ($_FILES['img']) {

            $extension = strtolower(substr(strrchr($_FILES['img']['name'], '.'), 1));

            Yii::import("application.helpers.UploadImgHelper", true);
            $fileName = UploadImgHelper::getUniqeFilename($this->userId, $extension);
            $filePath = UploadImgHelper::getPathRealImage($fileName);

            $res = copy($_FILES['img']['tmp_name'], $filePath);
            $model->img = $fileName;
        } else {
            $this->_sendResponse(500, sprintf('Parameter <b>img</b> is not > is not not available'));
        }

        $model->author_id = $this->userId;

        if ($model->save()) {
            $this->createThumb($filePath, $fileName);
            $this->_sendResponse(200, CJSON::encode($model));
        } else {
            // Errors occurred
            $msg = "<h1>Error</h1>";
            $msg .= sprintf("Couldn't create model <b>%s</b>", $_GET['model']);
            $msg .= "<ul>";
            foreach ($model->errors as $attribute => $attr_errors) {
                $msg .= "<li>Attribute: $attribute</li>";
                $msg .= "<ul>";
                foreach ($attr_errors as $attr_error)
                    $msg .= "<li>$attr_error</li>";
                $msg .= "</ul>";
            }
            $msg .= "</ul>";
            $this->_sendResponse(500, $msg);
        }
    }

    public function actionUpdate() {
        $this->_checkAuth();

        $json = Yii::app()->request->rawBody;
        $put_vars = CJSON::decode($json, true);  //true means use associative array
        //"{"email":"olenichphp@gmail.com2","username":"asdfasfd", "password":"asdfa"}"

        switch ($_GET['model']) {
            // Find respective model
            case 'posts':
                $model = TblPost::model()->findByPk($_GET['id'], 'author_id=:author_id', array(':author_id' => $this->userId));
                break;
            case 'users':
                $model = TblUser::model()->findByPk($_GET['id'], 'id=:id', array(':id' => $this->userId));
                break;
            default:
                $this->_sendResponse(501, sprintf('Error: Mode <b>update</b> is not implemented for model <b>%s</b>', $_GET['model']));
                Yii::app()->end();
        }

        if ($model === null)
            $this->_sendResponse(400, sprintf("Error: Didn't find any model <b>%s</b> with ID <b>%s</b>.", $_GET['model'], $_GET['id']));

        switch ($_GET['model']) {
            case 'users':
                if ($put_vars['username'])
                    $model->username = $put_vars['username'];
                if ($put_vars['email'])
                    $model->email = $put_vars['email'];
                if ($put_vars['password'])
                    $model->password = $model->hashPassword($put_vars['password']);
                break;
            case 'posts':
                if ($put_vars['title'])
                    $model->title = $put_vars['title'];
                if ($put_vars['price'])
                    $model->price = $put_vars['price'];
                break;
        }

        if ($model->save())
            $this->_sendResponse(200, CJSON::encode($model));
        else {
            // Errors occurred
            $msg = "<h1>Error</h1>";
            $msg .= sprintf("Couldn't create model <b>%s</b>", $_GET['model']);
            $msg .= "<ul>";
            foreach ($model->errors as $attribute => $attr_errors) {
                $msg .= "<li>Attribute: $attribute</li>";
                $msg .= "<ul>";
                foreach ($attr_errors as $attr_error)
                    $msg .= "<li>$attr_error</li>";
                $msg .= "</ul>";
            }
            $msg .= "</ul>";
            $this->_sendResponse(500, $msg);
        }
    }

    public function actionDelete() {

        $this->_checkAuth();
        switch ($_GET['model']) {
            case 'posts':
                $model = TblPost::model()->findByPk($_GET['id'], 'author_id=:author_id', array(':author_id' => $this->userId));
                break;
            default:
                $this->_sendResponse(501, sprintf('Error: Mode <b>delete</b> is not implemented for model <b>%s</b>', $_GET['model']));
                Yii::app()->end();
        }

        if ($model === null)
            $this->_sendResponse(400, sprintf("Error: Didn't find any model <b>%s</b> with ID <b>%s</b>.", $_GET['model'], $_GET['id']));

        // Delete the model
        $num = $model->delete();
        if ($num > 0)
            $this->_sendResponse(200, $num);    //this is the only way to work with backbone
        else
            $this->_sendResponse(500, sprintf("Error: Couldn't delete model <b>%s</b> with ID <b>%s</b>.", $_GET['model'], $_GET['id']));
    }

    private function _sendResponse($status = 200, $body = '', $content_type = 'text/html') {
        // set the status
        $status_header = 'HTTP/1.1 ' . $status . ' ' . $this->_getStatusCodeMessage($status);
        header($status_header);
        // and the content type
        header('Content-type: ' . $content_type);

        // pages with body are easy
        if ($body != '') {
            // send the body
            echo $body;
        }
        // we need to create the body if none is passed
        else {
            // create some body messages
            $message = '';

            // this is purely optional, but makes the pages a little nicer to read
            // for your users.  Since you won't likely send a lot of different status codes,
            // this also shouldn't be too ponderous to maintain
            switch ($status) {
                case 401:
                    $message = 'You must be authorized to view this page.';
                    break;
                case 404:
                    $message = 'The requested URL ' . $_SERVER['REQUEST_URI'] . ' was not found.';
                    break;
                case 500:
                    $message = 'The server encountered an error processing your request.';
                    break;
                case 501:
                    $message = 'The requested method is not implemented.';
                    break;
            }

            // servers don't always have a signature turned on 
            // (this is an apache directive "ServerSignature On")
            $signature = ($_SERVER['SERVER_SIGNATURE'] == '') ? $_SERVER['SERVER_SOFTWARE'] . ' Server at ' . $_SERVER['SERVER_NAME'] . ' Port ' . $_SERVER['SERVER_PORT'] : $_SERVER['SERVER_SIGNATURE'];

            // this should be templated in a real-world solution
            $body = '
        <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
        <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
            <title>' . $status . ' ' . $this->_getStatusCodeMessage($status) . '</title>
        </head>
        <body>
            <h1>' . $this->_getStatusCodeMessage($status) . '</h1>
            <p>' . $message . '</p>
            <hr />
            <address>' . $signature . '</address>
        </body>
        </html>';

            echo $body;
        }
        Yii::app()->end();
    }

    private function _checkAuth() {
        // Check if we have the USERNAME and PASSWORD HTTP headers set?
        if (!(isset($_SERVER['HTTP_XEMAIL']) and isset($_SERVER['HTTP_XPASSWORD']))) {
            // Error: Unauthorized
            $this->_sendResponse(401);
        }
        $email = $_SERVER['HTTP_XEMAIL'];
        $password = $_SERVER['HTTP_XPASSWORD'];
        // Find the user
        $user = TblUser::model()->find('LOWER(email)=:email AND status=1', array(':email' => $email));

        if ($user === null) {
            // Error: Unauthorized
            $this->_sendResponse(401, 'Error: User Name is invalid');
        } else if (!$user->validatePassword($password)) {
            // Error: Unauthorized
            $this->_sendResponse(401, 'Error: User Password is invalid');
        } else
            $this->userId = $user->id;
    }

    private function _getStatusCodeMessage($status) {
        // these could be stored in a .ini file and loaded
        // via parse_ini_file()... however, this will suffice
        // for an example
        $codes = Array(
            200 => 'OK',
            400 => 'Bad Request',
            401 => 'Unauthorized',
            402 => 'Payment Required',
            403 => 'Forbidden',
            404 => 'Not Found',
            500 => 'Internal Server Error',
            501 => 'Not Implemented',
        );
        return (isset($codes[$status])) ? $codes[$status] : '';
    }

    protected function createThumb($filePath, $fileName) {
        $thumb = Yii::app()->phpThumb->create($filePath);
        $thumb->resize(100, 100);
        if ($thumb->save(UploadImgHelper::getPathThumb($fileName)))
            return true;
        else
            return false;
    }

}
