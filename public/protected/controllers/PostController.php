<?php

class PostController extends Controller {

    public function filters() {
        return array(
            'accessControl',
        );
    }

    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array(
                    'view',
                ),
                'roles' => array('guest'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'mygoods', 'delete', 'edit'),
                'roles' => array('user'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }
    
    /*
     * просмотр товаров
     */

    public function actionView() {

        $model = new TblPost();
        $model->with('author');
        $model->unsetAttributes();
        $model->dbCriteria->order='t.create_time DESC';

        $this->render('view', array('model' => $model));
    }
    
    /*
     * создание товара
     */

    public function actionCreate() {

        $model = new PostForm;
        $model->scenario = 'create';
        $this->performAjaxValidation($model);

        if (isset($_POST['PostForm'])) {
            $model->attributes = $_POST['PostForm'];

            if ($model->validate()) {
                $userId = Yii::app()->user->id;
                $model->img = CUploadedFile::getInstance($model, 'img');
                $extension = strtolower(substr(strrchr($model->img->name, '.'), 1));
                
                
                Yii::import("application.helpers.UploadImgHelper", true);
                $fileName = UploadImgHelper::getUniqeFilename($userId, $extension);
                $filePath = UploadImgHelper::getPathRealImage($fileName);

                if($model->img->saveAs($filePath)){
                    $tblPost = new TblPost();
                    $tblPost->title = $model->title;
                    $tblPost->price = $model->price;
                    $tblPost->img = $fileName;
                    $tblPost->author_id = $userId;
 
                    if($tblPost->save()){
                        $this->createThumb($filePath, $fileName);
                      
                        Yii::app()->user->setFlash('message', 
                                '<p class="text-success">Товар "'. $tblPost->title. '" добавлен</p>');
 
                         $this->redirect('mygoods');
                         Yii::app()->end();
                    } else {
                        Yii::app()->user->setFlash('message', 
                            '<p class="text-danger">Ошибка! Товар не создан!</p>');
                    }
                }else{
                    Yii::app()->user->setFlash('message', 
                            '<p class="text-danger">Ошибка! Товар не создан!</p>');
                }
            }
        }
        $this->render('create', array('model' => $model));
    }
    
     /*
     * редактирование товара
     */

    public function actionEdit() {

        $postId = Yii::app()->request->getQuery('id','');
        if($postId !=''){
            $tblPost = TblPost::model()->findByPk((int) $postId, 
                    'author_id=:author_id', 
                    array(':author_id' =>Yii::app()->user->id));
            if($tblPost){
                $model = new PostForm;
                $model->scenario = 'edit';
                $this->performAjaxValidation($model);
                $model->attributes = $tblPost->attributes;
 
                if (isset($_POST['PostForm'])) {
                    $model->unsetAttributes();
                    $model->attributes = $_POST['PostForm'];

                    if ($model->validate()) {
                        $userId = Yii::app()->user->id;

                        $tblPost->title = $model->title;
                        $tblPost->price = $model->price;

                        $tblPost->author_id = $userId;
                        $model->img = CUploadedFile::getInstance($model, 'img');
                        if($model->img){
                            $model->img = CUploadedFile::getInstance($model, 'img');
                            $extension = strtolower(substr(strrchr($model->img->name, '.'), 1));
                            Yii::import("application.helpers.UploadImgHelper", true);
                            $fileName = UploadImgHelper::getUniqeFilename($userId, $extension);
                            $filePath = UploadImgHelper::getPathRealImage($fileName);
                            $model->img->saveAs($filePath);
                            $this->createThumb($filePath,$fileName);
                            $tblPost->img = $fileName;
                        }

                        if($tblPost->save()){
                            Yii::app()->user->setFlash('message', 
                                    '<p class="text-success">Товар "'. $tblPost->title. '" изменен</p>');
                             $this->redirect('/post/mygoods');
                             Yii::app()->end();
                        } else {
                            Yii::app()->user->setFlash('message', 
                                '<p class="text-danger">Ошибка! Товар не изменен!</p>');
                        }
                    }
                }
 
                $this->render('edit', array('model' => $model));
                Yii::app()->end();
   
            } else
                throw new CHttpException(400,'Неправильная ссылка.');
        }else
            throw new CHttpException(400,'Неправильная ссылка.');
    }   
    
     /*
     * удаление товара
     */

    public function actionDelete() {

        if (Yii::app()->request->isPostRequest) {

            $id = Yii::app()->request->getQuery('id','');
            if($id !== ''){
                

                $condition = 'author_id=:author_id';
                $params = array(':author_id' => Yii::app()->user->getId());
                $tblPost = TblPost::model()->findByPk((int) $id, $condition, $params)->delete();

                echo '<p class="text-success">Товар успешно удален</p>';
            }
        } else
        throw new CHttpException(400,'Неправильная ссылка.');
    }
    
    /*
     * Список товаров пользователя
     */

    public function actionMygoods() {

        $model = new TblPost();
        $model->unsetAttributes();
        $model->dbCriteria->order='t.create_time DESC';
        $model->author_id = Yii::app()->user->getId();

        $this->render('mygoods', array('model' => $model));
    }
    
    
    /**
     * Performs the AJAX validation.
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && ($_POST['ajax'] === 'post-form' || $_POST['ajax'] === 'endregistretion-form')) {
            $model->validate();
            Yii::app()->end();
        }
    }
    
    
    protected   function createThumb($filePath,$fileName){
        $thumb=Yii::app()->phpThumb->create($filePath);
        $thumb->resize(100,100);
        if($thumb->save(UploadImgHelper::getPathThumb($fileName)))
            return true;
        else 
            return false;
    }
}
