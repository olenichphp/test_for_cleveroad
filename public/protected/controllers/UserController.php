<?php

class UserController extends Controller {

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array(
                    'registration', 'activate'
                ),
                'roles' => array('guest'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('settings'),
                'roles' => array('user'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /*
     * Регистрация пользователя с валидацией
     */

    public function actionRegistration() {

        $model = new RegistrationForm;
        $model->scenario = 'register';

        $this->performAjaxValidation($model);

        if (isset($_POST['RegistrationForm'])) {
            $model->attributes = $_POST['RegistrationForm'];
            if ($model->validate()) {
                $tblUser = new TblUser;
                $tblUser->attributes = $model->attributes;
                $tblUser->attributes = array(
                    'password' => $tblUser->hashPassword($tblUser->attributes['password']));
                $tblUser->hash = md5(microtime() . rand(0, 9999));
                if ($tblUser->save()) {
                    if ($this->sendActiveMail(
                                    $tblUser->id, $tblUser->email, $tblUser->username, $tblUser->hash)) {

                        Yii::app()->user->setFlash('message', '<p class="bg-success">На указанный почтовый ящик '
                                . 'отправлено письмо с ссылкой для активации вашего '
                                . 'аккаунта.</p>');
                        $this->render('info');
                        Yii::app()->end();
                    } else
                        Yii::app()->user->setFlash('message', '<p class="bg-danger">Вы не правильно указали почту!</p>');
                }
            }
        }
        $this->render('registration', array('model' => $model));
    }

    /*
     * Активация пользователя
     *
     */

    public function actionActivate() {

        $messageOk = '<span class="text-success">Ваш аккаунт создан!</span>';
        $messageError = '<span class="text-danger">Неправильная ссылка!</span>';
        if (isset($_GET['id']) AND isset($_GET['hash'])) {

            $userId = (int) $_GET['id'];

            $user = TblUser::model()->findByPk($userId);
            if ($user) {
                if ($user->status == 0) {
                    if ($user->hash == $_GET['hash']) {
                        $user->hash = '';
                        $user->status = 1;
                        $user->save();
                        Yii::app()->user->setFlash('message', $messageOk);
                    } else {
                        Yii::app()->user->setFlash('message', $messageError);
                    }
                } else {
                    Yii::app()->user->setFlash('message', $messageOk);
                }
            } else {
                Yii::app()->user->setFlash('message', $messageError);
            }
        } else {
            Yii::app()->user->setFlash('message', $messageError);
        }
        $this->render('activate');
    }

    /**
     * изменение настроек пользователя
     */
    public function actionSettings() {
        $model = new RegistrationForm;
        $model->scenario = 'settings';

        $this->performAjaxValidation($model);

        $user = TblUser::model()->findByPk(Yii::app()->user->id);

        $model->attributes = $user->attributes;

        if (isset($_POST['RegistrationForm'])) {
            $model->unsetAttributes();
            $model->attributes = $_POST['RegistrationForm'];

            if ($model->validate()) {

                $user->username = $model->username;
                $user->email = $model->email;
                $user->password = $user->hashPassword($model->password);

                if ($user->save()) {
                    Yii::app()->user->setFlash('message', '<p class="text-success"><b>Данные обновлены</b></p>');
                    $this->refresh();
                } else {
                    Yii::app()->user->setFlash('message', '<p class="text-danger">Не удалось обновить данные, обратитесь в службу поддержки!</p>');
                    $this->refresh();
                }
            }
        }
        $this->render('settings', array('model' => $model));
    }

    /*
     * отправляет письмо со ссылкой для активации аккаунта
     */

    protected function sendActiveMail($id, $mail, $name, $hash) {
        $message = "$name, вы подали заявку на регистрацию на Доске объявлений. " .
                "Подтвердите свою заявку по предложенной ссылке: " .
                "http://phptest.in.ua/user/activate?id=$id&hash=" . $hash;
        $subject = "Подтверждение регистрации";
        $from = Yii::app()->params->adminEmail;
        if (mail($mail, $subject, $message, 'From: ' . $from)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Performs the AJAX validation.
     * @param Users $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && ($_POST['ajax'] === 'registration-form' || $_POST['ajax'] === 'endregistretion-form')) {
            $model->validate();
            Yii::app()->end();
        }
    }

}
