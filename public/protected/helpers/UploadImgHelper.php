<?php

/*
 * загрузка изображений
 */

class UploadImgHelper {
    
    const UPLOADPATH = '/uploads/catalog/';
    const UPLOADTHUMB = '/uploads/catalog/thumb/';

     /**
     * Возвращает уникальное имя файла
     */
    public static function getUniqeFilename($userId, $extension) {
        $extension = $extension ? '.' . $extension : '';
        $path = self::getPathRealImage();
        do {
            $name = substr(md5($userId . microtime()), 20). $extension;
            $file = $path . $name ;
        } while (file_exists($file));
 
        return $name; 
    }
    
     /**
     * Возвращает путь к большим изображениям
     */
    public static function getPathRealImage($fileName='') {

        return Yii::getPathOfAlias('webroot'). self::UPLOADPATH . ($fileName ? $fileName : ''); 
    }
    
    /**
     * Возвращает путь к миниатюрам
     */
    public static function getPathThumb($fileName='') {

        return Yii::getPathOfAlias('webroot').self::UPLOADTHUMB . ($fileName ? $fileName : ''); 
    }
}