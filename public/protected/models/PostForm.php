<?php

class PostForm extends CFormModel {

    public $title;
    public $price;
    public $img;
    
    /**
     * Declares the validation rules.
     */
    public function rules() {
        return array(
          
            // обязательные поля
            array('title,  price',
                'required',
                'message' => 'Введите {attribute}!',
                'on' => 'create, edit',
            ),
            array('price',
                'numerical',
                'integerOnly' => false,
                'max'         => 999999,
                'tooBig'      => 'Не более 999999',
                'message'     => 'Введите число цифрами',
            ),
            array('img', 'file',
                'allowEmpty' => false,
                'types' => 'jpeg, png',
                'maxSize' => 5242880, //1024*1024*5
                'tooLarge' => 'Слишком большй файл (больше 5Мбайта',
                'wrongType' => 'Выберите файл с расширением &#46;jpeg,  &#46;png!',
                'safe'=>true,
               'on' => 'create',
            ),
            array('img', 'file',
                'allowEmpty' => true,
                'types' => 'jpeg, png',
                'maxSize' => 5242880, //1024*1024*5
                'tooLarge' => 'Слишком большй файл (больше 5Мбайта',
                'wrongType' => 'Выберите файл с расширением &#46;jpeg,  &#46;png!',
                'safe'=>true,
               'on' => 'edit',
            ),
 
        );
    }

    /**
     * Declares attribute labels.
     */
    public function attributeLabels() {
        return array(
            'title' => 'название товара',
            'price' => 'цена',
            'img' => 'файл с изображением',
        );
    }
        
    public function getPathImg($filename){
        Yii::import("application.helpers.UploadImgHelper", true); 
        return UploadImgHelper::UPLOADPATH . $filename;
    }
}
