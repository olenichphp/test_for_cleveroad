<?php

class RegistrationForm extends CFormModel {

    public $username;
    public $email;
    public $password;
    public $confirmPassword;

    /**
     * Declares the validation rules.
     */
    public function rules() {
        return array(
            //проверка уникальности логина
            array('email',
                'unique',
                'allowEmpty' => false,
                'className' => 'TblUser',
                'attributeName' => 'email',
                'caseSensitive' => FALSE,
                'message' => 'Эта почта уже используется, выберите другую!',
                'on' => 'register'
            ),
            array('email',
                'uniqueEmail',
                'on' => 'settings'
            ),
            array('username',
                'unique',
                'allowEmpty' => false,
                'className' => 'TblUser',
                'attributeName' => 'username',
                'caseSensitive' => FALSE,
                'message' => 'Это имя уже используется, выберите другое!',
                'on' => 'register'
            ),
            array('username',
                'uniqueUsername',
                'on' => 'settings'
            ),
            // обязательные поля
            array('username,  email,password,confirmPassword',
                'required',
                'message' => 'Введите {attribute}!',
                'on' => 'register, settings',
            ),
            //проверка на email
            array('email',
                'email',
                'message' => 'Введите корректный e-mail!'
            ),
            //проверка на длину поля
            array('username, password, confirmPassword',
                'length',
                'min' => 4,
                'max' => 30,
                'tooShort' => 'Введите не менее 4ти символов!'),     
            //идентификация пароля
            array('confirmPassword',
                'compare',
                'compareAttribute' => 'password',
                'message' => 'Пароли не совпадают!'),
 
        );
    }

    /**
     * Declares attribute labels.
     */
    public function attributeLabels() {
        return array(
            'username' => 'имя',
            'password' => 'пароль',
            'confirmPassword' => 'пароль повторно',
            'email' => 'email',
        );
    }
 

    /**
     * проверяет уникальность email для зарегистрированного пользователя
     */
    public function uniqueEmail($attribute, $params) {
        $user = TblUser::model()->find('email=:email AND id!=:id', 
                array(':email' => $this->email, ':id' => Yii::app()->user->id));
        if ($user !== null) {
            $this->addError($attribute, 'Такой email существует!');
        }
    }
    
    /**
     * проверяет уникальность username для зарегистрированного пользователя
     */
    public function uniqueUsername($attribute, $params) {
        $user = TblUser::model()->find('username=:username AND id!=:id', 
                array(':username' => $this->username, ':id' => Yii::app()->user->id));
        if ($user !== null) {
            $this->addError($attribute, 'Такое имя существует!');
        }
    }
}
