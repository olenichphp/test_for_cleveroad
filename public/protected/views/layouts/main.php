<?php /* @var $this Controller */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />
        <link rel="stylesheet" type="text/css" href="<?php //echo Yii::app()->request->baseUrl; ?>/css/style.css" />

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body>

<div class="container" id="page">

	<div id="header">
		<div id="logo"><?php echo CHtml::encode(Yii::app()->name); ?></div>
	</div><!-- header -->

	<div id="mainmenu">
            <?php $this->widget('UserMenu'); ?><!-- mainmenu -->
	</div><!-- mainmenu -->
                  <?php if (isset($this->breadcrumbs)): 
                        $this->widget('zii.widgets.CBreadcrumbs', array(
                            'links'                => $this->breadcrumbs,
                            'homeLink'             => CHtml::tag('li') . CHtml::link('Главная', Yii::app()->homeUrl),
                            'tagName'              => 'ol',
                            'activeLinkTemplate'   => '<li><a href={url}>{label}</a></li>',
                            'inactiveLinkTemplate' => '<li>{label}</li>',
                            'separator'            => '',
                            'htmlOptions'          => array('class' => 'breadcrumb'),
                        ));
                       endif ?>

	<?php echo $content; ?>

	<div class="clear"></div>

	
        <footer>
		<?php echo Yii::powered(); ?>
        </footer>

</div><!-- page -->

</body>
</html>
