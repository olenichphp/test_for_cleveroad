<?php
/* @var $this PostController */
/* @var $model TblPost */
?>
<div class="row">
     <div class="col-xs-8 text-center"  id="message-info">
            <?php if (Yii::app()->user->hasFlash('message')) {
                echo Yii::app()->user->getFlash('message');
            }
            ?>
    </div>
    <div class="col-xs-3">
        <?php echo CHtml::button('Создать товар',
    array(
        'submit'=>array('post/create'),
        'class' => 'btn btn-primary'
    )); 
        ?>
    </div> 
</div>
<?php
$form = $this->widget('zii.widgets.grid.CGridView', array(
    'dataProvider' => $model->search(),
    'pager' => array(
        'maxButtonCount' => 6, //количество страниц в пагинаторе
        'header' => '',
        'selectedPageCssClass' => 'active',
        'hiddenPageCssClass' => false,
        'firstPageLabel' => '&laquo;&laquo;',
        'prevPageLabel' => '&laquo;',
        'nextPageLabel' => '&raquo;',
        'lastPageLabel' => '&laquo;&laquo;',
        'htmlOptions' => array('class' => 'pagination'),
    ),
    'columns' => array(
        array(
            'name' => 'img',
            'header' => 'Изображение',
            'type' => 'image',
            'sortable' => true,
            //'filter' => CHtml::activeTextField($model, 'group_name', array('class' => 'form-control')),
            //'headerHtmlOptions' => array('width' => '20%',),
            'value' => '$data->getPathThumbl($data->img)',
        ),
        array(
            'name' => 'title',
            'header' => 'Название',
            'type' => 'raw',
            'sortable' => true,
            'headerHtmlOptions' => array('width' => '40%',),
            'value' => '$data->getTitleLink($data->title, $data->id)',
        ),
        array(
            'name' => 'create_time',
            'header' => 'Дата создания',
            'type' => 'raw',
            'sortable' => true,
            'htmlOptions' => array('style' => 'text-align:center'),
            'headerHtmlOptions' => array('width' => '20%',),
        ), 
        array(
            'name' => 'price',
            'header' => 'Цена',
            'type' => 'raw',
            'sortable' => true,
            'htmlOptions' => array('style' => 'text-align:center'),
            'headerHtmlOptions' => array('width' => '15%',),
        ),

        array(
            'class' => 'CButtonColumn',
            'template' => '{update}&nbsp;&nbsp;&nbsp;{delete}',
            'deleteConfirmation' => 'Удалить товар?',
            //'afterDelete' => 'function(link,success,data){ if(success) alert(data); }',
            'afterDelete' => "function(link,success,data){ if(success)  {  "
            . "document.getElementById('message-info').innerHTML = data;} }",
            'buttons' => array(
                'update' => array(
                    'label' => 'редактировать товар', //Text label of the button.
                    'url' => 'Yii::app()->createUrl("/post/edit", array("id"=> $data->id))',
                //'imageUrl'=>false,  //Image URL of the button.
                //'options'=>array('class' =>'glyphicon glyphicon-search'), //HTML options for the button tag.
                //'click'=>'...',     //A JS function to be invoked when the button is clicked.
                // 'visible'=>'...',   //A PHP expression for determining whether the button is visible.
                ),
                'delete' => array(
                    'label' => 'Удалить товар', //'Смотреть отчет',     //Text label of the button.
                    'url' => 'Yii::app()->createUrl("/post/delete", array("id"=> $data->id))',
                //'imageUrl'=>'/images/icon/excel.png',  //Image URL of the button.
                //'options'=>array('class' =>'glyphicon glyphicon-search'), //HTML options for the button tag.
                //'click'=>'',     //A JS function to be invoked when the button is clicked.
                // 'visible'=>'...',   //A PHP expression for determining whether the button is visible.
                ),
            ),
            'htmlOptions' => array('style' => 'text-align:center'),
            'headerHtmlOptions' => array('width' => '10%'),
        ),

    ),
    'emptyText' => 'Ничего не найдено!',
    'template' => '{items}<div class="row">{summary} {pager} </div>',
    'summaryCssClass' => 'col-sm-5',
    'pagerCssClass' => 'col-sm-7',
    'summaryText' => 'Всего {count} строк. Показано {start} - {end}',
));
