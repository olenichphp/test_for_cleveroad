<?php
/* @var $this PostController */
/* @var $model TblPost */
?>
<div class="row">
     <div class="col-xs-8 text-center"  id="message-info">
            <?php if (Yii::app()->user->hasFlash('message')) {
                echo Yii::app()->user->getFlash('message');
            }
            ?>
    </div>
</div>

<?php

$form = $this->widget('zii.widgets.grid.CGridView', array(
    'dataProvider' => $model->search(),
    'pager' => array(
        'maxButtonCount' => 6, //количество страниц в пагинаторе
        'header' => '',
        'selectedPageCssClass' => 'active',
        'hiddenPageCssClass' => false,
        'firstPageLabel' => '&laquo;&laquo;',
        'prevPageLabel' => '&laquo;',
        'nextPageLabel' => '&raquo;',
        'lastPageLabel' => '&laquo;&laquo;',
        'htmlOptions' => array('class' => 'pagination'),
    ),
    'columns' => array(
        array(
            'name' => 'img',
            'header' => 'Изображение',
            'type' => 'image',
            'sortable' => true,
            //'filter' => CHtml::activeTextField($model, 'group_name', array('class' => 'form-control')),
            //'headerHtmlOptions' => array('width' => '20%',),
            'htmlOptions' => array('class' => 'text-center'),
            'value' => '$data->getPathThumbl($data->img)',
        ),
        array(
            'name' => 'title',
            'header' => 'Название',
            'type' => 'raw',
            'sortable' => true,
            'headerHtmlOptions' => array('width' => '40%',),
            'value' => '$data->title',
        ),
        array(
            'name' => 'create_time',
            'header' => 'Дата создания',
            'type' => 'raw',
            'sortable' => true,
            'htmlOptions' => array('class' => 'text-center'),
            'headerHtmlOptions' => array('width' => '20%',),
        ), 
        array(
            'name' => 'price',
            'header' => 'Цена',
            'type' => 'raw',
            'sortable' => true,
            'htmlOptions' => array('class' => 'text-center'),
            'headerHtmlOptions' => array('width' => '15%',),
        ),
        array(
            'name' => 'email',
            'header' => 'email владельца',
            'type' => 'raw',
            'htmlOptions' => array('class' => 'text-center'),
            'headerHtmlOptions' => array('width' => '15%',),
            'value' => '$data->author->email',
        ),
    ),
    'emptyText' => 'Ничего не найдено!',
    'template' => '{items}<div class="row">{summary} {pager} </div>',
    'summaryCssClass' => 'col-sm-5',
    'pagerCssClass' => 'col-sm-7',
    'summaryText' => 'Всего {count} строк. Показано {start} - {end}',
));
