<?php
/* @var $this PostController */
/* @var $model PostForm */
$this->pageTitle=Yii::app()->name . ' - Редактировать товар';
$this->breadcrumbs=array(
	'Редактировать товар',
);
?>

<div class="col-xs-9 col-xs-offset-3">&nbsp
        <?php if (Yii::app()->user->hasFlash('message')) {
            echo Yii::app()->user->getFlash('message');
        }
        ?></div>
<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'post-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
            'validateOnSubmit'=>true,
            'validateOnChange'=>true,
            'errorCssClass' => 'error',
            'successCssClass' => 'success',
	),
        'htmlOptions' => array('class' => 'form-horizontal',
            'enctype'=> 'multipart/form-data')
)); ?>
    <div class="form-group">
        <?php
        echo $form->labelEx($model, 'Название товара:', array(
            'class' => 'col-xs-4 control-label'));
        ?>
        <div class="col-xs-4">
            <?php
            echo $form->textField($model, 'title', array(
                'maxlength' => 256,
                'class' => 'form-control',
                'placeholder' => 'Название товара',
                'autocomplete' => 'off'));
            ?>
        </div>
        <div class="col-xs-4">
            <?php
            echo $form->error($model, 'title', array('class' => 'text-danger'));
            ?>
        </div>
    </div>

    <div class="form-group">
        <?php
        echo $form->labelEx($model, 'Цена товара, грн.:', array(
            'class' => 'col-xs-4 control-label'));
        ?>
        <div class="col-xs-4">
            <?php
            echo $form->textField($model, 'price', array(
                'maxlength' => 20,
                'class' => 'form-control',
                'placeholder' => 'цена товара'));
            ?>
        </div>
        <div class="col-xs-4">
            <?php
            echo $form->error($model, 'price', array('class' => 'text-danger'));
            ?>
        </div>
    </div>
   
    <div class="form-group">
        <?php
        echo $form->labelEx($model, 
                'Фото товара  в формате &#46;jpeg, &#46;png:', 
                array(
            'class' => 'col-xs-4 control-label'));
        ?>
        <div class="col-xs-4">
            <?php
            echo $form->fileField($model, 'img', array(
                //'class' => 'file',
                ));
            ?>
        </div>
        <div class="col-xs-4">
            <?php
            echo $form->error($model, 'img', array('class' => 'text-danger'));
            ?>
        </div>
    </div>
    <div class="text-center">
        <img class="image-limit" src="<?=Yii::app()->request->baseUrl.$model->getPathImg($model->img); ?>"/>
    </div>
    <div class="row buttons">
        <div class="col-xs-2 col-xs-offset-5">
             <?php echo CHtml::submitButton('Сохранить изменения', array(
                 'class' => 'btn btn-primary',
                 'name' => 'edit')); ?>
        </div>
    </div>


<?php $this->endWidget(); ?>
</div>
 
