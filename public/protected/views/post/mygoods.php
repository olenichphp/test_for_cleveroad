<?php
/* @var $this PostController */
/* @var $model TblPost */
$this->pageTitle=Yii::app()->name . ' - Мои товары';
$this->breadcrumbs=array(
	'Мои товары',
);

$this->renderPartial('_mygoods', array('model' => $model));