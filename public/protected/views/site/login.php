<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle = Yii::app()->name . ' - Вход';
$this->breadcrumbs = array(
    'Вход',
);
?>

<div class="form">
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'login-form',
        'htmlOptions' => array('class' => 'form-horizontal'),
        'id' => 'login-form',
        'enableClientValidation' => true,
        'enableAjaxValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'validateOnChange' => false,
            'errorCssClass' => 'has-error',
            'successCssClass' => 'has-success',
        ),
    ));
    ?>
    <div class="form-group">
        <?php
        echo $form->labelEx($model, 'email:', array(
            'class' => 'col-xs-4 control-label'));
        ?>
        <div class="col-xs-4">
            <?php
            echo $form->textField($model, 'email', array(
                'maxlength' => 30,
                'class' => 'form-control',
                'placeholder' => 'Логин',));
            ?>
        </div>
        <div class="col-xs-4">
            <?php
            echo $form->error($model, 'email', array('class' => 'text-danger'));
            ?>
        </div>
    </div>
    <div class="form-group">
        <?php
        echo $form->labelEx($model, 'Пароль:', array(
            'class' => 'col-xs-4 control-label'));
        ?>
        <div class="col-xs-4">
            <?php
            echo $form->passwordField($model, 'password', array(
                'maxlength' => 30,
                'class' => 'form-control',
                'placeholder' => 'Пароль',
                'value' => ''));
            ?>
        </div>
        <div class="col-xs-4">
            <?php
            echo $form->error($model, 'password', array('class' => 'text-danger'));
            ?>
        </div>
    </div>
    <div class="row buttons">
        <div class="col-xs-2 col-xs-offset-5">
             <?php echo CHtml::submitButton('Войти', array(
                 'class' => 'btn btn-primary',
                 'name' => 'register')); ?>
        </div>
    </div>

<?php $this->endWidget(); ?>
</div> 
