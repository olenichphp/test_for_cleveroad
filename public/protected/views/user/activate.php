<?php
/* @var $this UserController */
$this->pageTitle = Yii::app()->name . ' - Активация профиля';
$this->breadcrumbs = array(
    'Активация профиля',
);
?>
<div class="text-center">
    <?php
    if (Yii::app()->user->hasFlash('message')) {
        echo Yii::app()->user->getFlash('message');
    }
    ?>
</div>