<?php
/* @var $this SiteController */
$this->pageTitle = Yii::app()->name . ' - Регистрация';
$this->breadcrumbs = array(
    'Регистрация',
);
?>
<div class="text-center">
    <?php
    if (Yii::app()->user->hasFlash('message')) {
        echo Yii::app()->user->getFlash('message');
    }
    ?>
</div>