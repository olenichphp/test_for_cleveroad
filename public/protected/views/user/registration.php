<?php
/* @var $this UserController */
/* @var $model RegistrationForm */
$this->pageTitle=Yii::app()->name . ' - Регистрация';
$this->breadcrumbs=array(
	'Регистрация',
);

?>
<div class="col-xs-9 col-xs-offset-3">&nbsp
        <?php if (Yii::app()->user->hasFlash('message')) {
            echo Yii::app()->user->getFlash('message');
        }
        ?></div>
<div class="form registration">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'registration-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
            'validateOnSubmit'=>true,
            'validateOnChange'=>true,
            'errorCssClass' => 'error',
            'successCssClass' => 'success',
	),
        'htmlOptions' => array('class' => 'form-horizontal')
)); ?>
    <div class="form-group">
        <?php
        echo $form->labelEx($model, 'Имя:', array(
            'class' => 'col-xs-4 control-label'));
        ?>
        <div class="col-xs-4">
            <?php
            echo $form->textField($model, 'username', array(
                'maxlength' => 30,
                'class' => 'form-control',
                'placeholder' => 'Имя',
                'autocomplete' => 'off'));
            ?>
        </div>
        <div class="col-xs-4">
            <?php
            echo $form->error($model, 'username', array('class' => 'text-danger'));
            ?>
        </div>
    </div>

    <div class="form-group">
        <?php
        echo $form->labelEx($model, 'Email:', array(
            'class' => 'col-xs-4 control-label'));
        ?>
        <div class="col-xs-4">
            <?php
            echo $form->textField($model, 'email', array(
                'maxlength' => 30,
                'class' => 'form-control',
                'placeholder' => 'email'));
            ?>
        </div>
        <div class="col-xs-4">
            <?php
            echo $form->error($model, 'email', array('class' => 'text-danger'));
            ?>
        </div>
    </div>
   
    <div class="form-group">
        <?php
        echo $form->labelEx($model, 'Пароль:', array(
            'class' => 'col-xs-4 control-label'));
        ?>
        <div class="col-xs-4">
            <?php
            echo $form->passwordField($model, 'password', array(
                'maxlength' => 30,
                'class' => 'form-control',
                'placeholder' => 'Пароль',
                'value' => ''));
            ?>
        </div>
        <div class="col-xs-4">
            <?php
            echo $form->error($model, 'password', array('class' => 'text-danger'));
            ?>
        </div>
    </div>
   
    <div class="form-group">
        <?php
        echo $form->labelEx($model, 'Пароль (повторно):', array(
            'class' => 'col-xs-4 control-label'));
        ?>
        <div class="col-xs-4">
            <?php
            echo $form->passwordField($model, 'confirmPassword', array(
                'maxlength' => 30,
                'class' => 'form-control',
                'placeholder' => 'Пароль',
                'value' => ''));
            ?>
        </div>
        <div class="col-xs-4">
            <?php
            echo $form->error($model, 'confirmPassword', array('class' => 'text-danger'));
            ?>
        </div>
    </div>
    <div class="row">
        <p class="col-xs-offset-4">Все поля обязательные для заполнения!</p>
    </div>
    <div class="row buttons">
        <div class="col-xs-2 col-xs-offset-5">
             <?php echo CHtml::submitButton('Зарегистрироваться', array(
                 'class' => 'btn btn-primary',
                 'name' => 'register')); ?>
        </div>
    </div>
<?php $this->endWidget(); ?>
</div>
